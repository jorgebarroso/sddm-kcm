# Burkhard Lück <lueck@hube-lueck.de>, 2014, 2017, 2018, 2019, 2020, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-11 01:00+0000\n"
"PO-Revision-Date: 2022-03-13 09:39+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: sddmauthhelper.cpp:371 sddmauthhelper.cpp:393 sddmauthhelper.cpp:400
msgid "Invalid theme package"
msgstr "Ungültiges Designpakets"

#: sddmauthhelper.cpp:377
msgid "Could not open file"
msgstr "Datei kann nicht geöffnet werden"

#: sddmauthhelper.cpp:408
msgid "Could not decompress archive"
msgstr "Das Archive kann nicht entpackt werden"

#: sddmthemeinstaller.cpp:29
#, kde-format
msgid "SDDM theme installer"
msgstr "Installationsprogramm für SDDM-Designs"

#: sddmthemeinstaller.cpp:36
#, kde-format
msgid "Install a theme."
msgstr "Installiert ein Design."

#: sddmthemeinstaller.cpp:37
#, kde-format
msgid "Uninstall a theme."
msgstr "Deinstalliert ein Design"

#: sddmthemeinstaller.cpp:39
#, kde-format
msgid "The theme to install, must be an existing archive file."
msgstr "Das zu installierende Design muss eine vorhandene Archivdatei sein."

#: sddmthemeinstaller.cpp:65
#, kde-format
msgid "Unable to install theme"
msgstr "Das Design kann nicht installiert werden"

#: src/package/contents/ui/Advanced.qml:17
#, kde-format
msgctxt "@title"
msgid "Behavior"
msgstr "Verhalten"

#: src/package/contents/ui/Advanced.qml:22
#, kde-format
msgctxt "option:check"
msgid "Automatically log in:"
msgstr "Automatisch anmelden:"

#: src/package/contents/ui/Advanced.qml:25
#, kde-format
msgctxt ""
"@label:listbox, the following combobox selects the user to log in "
"automatically"
msgid "as user:"
msgstr "als Benutzer:"

#: src/package/contents/ui/Advanced.qml:71
#, kde-format
msgctxt ""
"@label:listbox, the following combobox selects the session that is started "
"automatically"
msgid "with session"
msgstr "mit Sitzung"

#: src/package/contents/ui/Advanced.qml:103
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Auto-login does not support unlocking your KDE Wallet automatically, so it "
"will ask you to unlock it every time you log in."
msgstr ""

#: src/package/contents/ui/Advanced.qml:108
#, kde-format
msgid "Open KDE Wallet Settings"
msgstr ""

#: src/package/contents/ui/Advanced.qml:114
#, kde-format
msgctxt "@option:check"
msgid "Log in again immediately after logging off"
msgstr "Nach dem Abmelden sofort wieder anmelden"

#: src/package/contents/ui/Advanced.qml:127
#, kde-format
msgctxt "@label:spinbox"
msgid "Minimum user UID:"
msgstr "Minimale Benutzer-UID (Kennung):"

#: src/package/contents/ui/Advanced.qml:139
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum user UID:"
msgstr "Maximale Benutzer-UID (Kennung):"

#: src/package/contents/ui/Advanced.qml:154
#, kde-format
msgctxt "@label:textbox"
msgid "Halt Command:"
msgstr "Befehl zum Herunterfahren:"

#: src/package/contents/ui/Advanced.qml:185
#, kde-format
msgctxt "@label:textbox"
msgid "Reboot Command:"
msgstr "Befehl zum Neustart:"

#: src/package/contents/ui/DetailsDialog.qml:26
#, kde-format
msgctxt "@title:window, %1 is the theme name, %2 the version"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: src/package/contents/ui/DetailsDialog.qml:55
#, kde-format
msgid "No preview available"
msgstr "Keine Vorschau verfügbar"

#: src/package/contents/ui/DetailsDialog.qml:58
#, kde-format
msgctxt ""
"%1 is a description of the theme, %2 are the authors, %3 is the license"
msgid "%1, by %2 (%3)"
msgstr "%1, von %2 (%3)"

#: src/package/contents/ui/main.qml:65
#, kde-format
msgctxt "@info:tooltip"
msgid "View details"
msgstr "Details anzeigen"

#: src/package/contents/ui/main.qml:80
#, kde-format
msgctxt "@info:tooltip"
msgid "Change Background"
msgstr "Hintergrund ändern"

#: src/package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete"
msgstr "Löschen"

#: src/package/contents/ui/main.qml:105
#, kde-format
msgctxt "@action:button"
msgid "Behavior..."
msgstr "Verhalten ..."

#: src/package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button"
msgid "Apply Plasma Settings..."
msgstr "Plasma-Einstellungen anwenden ..."

#: src/package/contents/ui/main.qml:115
#, kde-format
msgctxt "@action:button"
msgid "Install From File..."
msgstr "Aus Datei installieren ..."

#: src/package/contents/ui/main.qml:120
#, kde-format
msgctxt "@action:button"
msgid "Get New SDDM Themes..."
msgstr "Neue SDDM-Designs holen ..."

#: src/package/contents/ui/main.qml:138
#, kde-format
msgctxt "@title:window"
msgid "Apply Plasma Settings"
msgstr "Plasma-Einstellungen anwenden"

#: src/package/contents/ui/main.qml:144
#, kde-format
msgid ""
"This will make the SDDM login screen reflect your customizations to the "
"following Plasma settings:"
msgstr ""

#: src/package/contents/ui/main.qml:145
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para><list><item>Color scheme,</item><item>Cursor theme,</item><item>Font,</"
"item><item>Font rendering,</item><item>NumLock preference,</"
"item><item>Plasma theme,</item><item>Scaling DPI,</item><item>Screen "
"configuration (Wayland only)</item></list></para>"
msgstr ""
"<para><list><item>Farbschema,</item><item>Zeigerdesign,</"
"item><item>Schriftarten,</item><item>Schrift-Rendering,</item><item>Zahlen-"
"Feststelltaste,</item><item>Plasma-Design</item><item>und DPI-Skalierung</"
"item> <item>Anzeige-Einstellungen (nur Wayland)</item></list></para>"

#: src/package/contents/ui/main.qml:146
#, fuzzy, kde-format
#| msgid ""
#| "Please note that theme files must be installed globally for settings "
#| "synchronization to work."
msgid ""
"Please note that theme files must be installed globally to be reflected on "
"the SDDM login screen."
msgstr ""
"Beachten Sie, dass die Designdatei global installiert sein muss, damit das "
"Abgleichen der Einstellungen funktioniert."

#: src/package/contents/ui/main.qml:153
#, kde-format
msgctxt "@action:button"
msgid "Apply"
msgstr "Anwenden"

#: src/package/contents/ui/main.qml:158
#, kde-format
msgctxt "@action:button"
msgid "Reset to Default Settings"
msgstr "Auf Standardeinstellung zurücksetzen"

#: src/package/contents/ui/main.qml:168
#, kde-format
msgctxt "@title:window"
msgid "Change Background"
msgstr "Hintergrund ändern"

#: src/package/contents/ui/main.qml:177
#, kde-format
msgid "No image selected"
msgstr "Es ist kein Bild ausgewählt"

#: src/package/contents/ui/main.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Load From File..."
msgstr "Aus Datei laden ..."

#: src/package/contents/ui/main.qml:208
#, kde-format
msgctxt "@action:button"
msgid "Clear Image"
msgstr "Bild löschen"

#: src/sddmkcm.cpp:155 src/sddmkcm.cpp:272
msgid ""
"Cannot proceed, user 'sddm' does not exist. Please check your SDDM install."
msgstr ""

#: src/sessionmodel.cpp:84
#, kde-format
msgctxt "%1 is the name of a session"
msgid "%1 (Wayland)"
msgstr "%1 (Wayland)"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Burkhard Lück"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lueck@hube-lueck.de"

#~ msgid "Login Screen (SDDM)"
#~ msgstr "Anmeldebildschirm (SDDM)"

#~ msgid "Login screen using the SDDM"
#~ msgstr "Anmeldebildschirm (SDDM)"

#~ msgid "Author"
#~ msgstr "Autor"

#~ msgctxt "@action:button"
#~ msgid "Synchronize Settings..."
#~ msgstr "Einstellungen abgleichen ..."

#~ msgctxt "@title:window"
#~ msgid "Settings Synchronization"
#~ msgstr "Abgleich der Einstellungen"

#~ msgid ""
#~ "Settings synchronization allows you to transfer the following Plasma "
#~ "settings to SDDM:"
#~ msgstr ""
#~ "Mit dem Abgleichen der Einstellungen können Sie folgende Plasma-"
#~ "Einstellungen auf SDDM übertragen:"

#~ msgctxt "@action:button"
#~ msgid "Synchronize"
#~ msgstr "Abgleichen"

#~ msgctxt "@action:button"
#~ msgid "Reset"
#~ msgstr "Zurücksetzen"

#~ msgid "SDDM Themes"
#~ msgstr "SDDM-Designs"

#~ msgid "SDDM KDE Config"
#~ msgstr "KDE-Einrichtung von SDDM"

#~ msgctxt "@title:window"
#~ msgid "Select Image"
#~ msgstr "Bild auswählen"

#~ msgid ", by "
#~ msgstr ", von"

#~ msgid "Theme"
#~ msgstr "Design"

#~ msgid "Advanced"
#~ msgstr "Erweitert"

#~ msgid "Sync"
#~ msgstr "Abgleichen"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Remove"
#~ msgstr "Entfernen"

#~ msgid "Get New Login Screens..."
#~ msgstr "Neue Anmeldebildschirme holen ..."

#~ msgid "Default"
#~ msgstr "Voreinstellung"

#~ msgid "The default cursor theme in SDDM"
#~ msgstr "Das Standard-Zeigerdesign in SDDM"

#~ msgid "Name"
#~ msgstr "Name"

#~ msgid "Description"
#~ msgstr "Beschreibung"

#~ msgctxt ""
#~ "@info The argument is the list of available sizes (in pixel). Example: "
#~ "'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
#~ msgid "(Available sizes: %1)"
#~ msgstr "(Verfügbare Größen: %1)"

#~ msgid "Cursor theme:"
#~ msgstr "Zeigerdesign:"

#~ msgid "General"
#~ msgstr "Allgemein"

#~ msgid "Auto &Login"
#~ msgstr "Automatische Anme&ldung"

#~ msgid "Relogin after quit"
#~ msgstr "Nach Beenden erneut anmelden"

#~ msgid "User"
#~ msgstr "Benutzer"

#~ msgid "Commands"
#~ msgstr "Befehle"

#~ msgid "Customize theme"
#~ msgstr "Design anpassen"

#~ msgctxt "Caption to theme preview, %1 author name"
#~ msgid "by %1"
#~ msgstr "von %1"
